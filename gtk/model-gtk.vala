namespace ModelGtk {
	/* The ListTracker is responsible for tracking a Model.List in a way that is convenient for a GtkTreeModel
	 * implementation.  It attempts to be lazy, minimising work where possible and only sending signals for
	 * things that an outside observer can know has changed.  For example, if an outside observer knows that the
	 * list has children but not how many and a new child is added, then no "insert" signal will be sent because
	 * as far as the outside observer knows, that child was always there.
	 *
	 * To this end, there are three states that a ListTracker can be in:
	 *
	 *   0 - Freshly created: this is the initial state.  The ListTracker is holding reference to a Model.List
	 *                        and watching for changes.  The ListTracker knows the length of the Model.List but
	 *                        has not exposed this information.  No child elements have been accessed.  At this
	 *                        point the only signal that will be sent on the model in response to changes is
	 *                        row_has_child_toggled, if appropriate.  Not sending remove (and specifically)
	 *                        insert signals from this state decreases the likelihood of a view making
	 *                        unnecessary query calls back into the model in response to those signals and
	 *                        forcing more of the model to be pulled into existence in order to reply to those
	 *                        queries.
	 *
	 *   1 - Length exposed:  this is the state that the ListTracker enters after the length has been exposed
	 *                        (ie: get_length() has been called).  In this state the ListTracker gains no
	 *                        additional internal information about the Model.List, but behaves differently with
	 *                        respect to signal emissions.  Specifically: insert and removes to the list are
	 *                        singled as inserts and removes on the GtkTreeModel so that the GtkTreeModel can
	 *                        know that the length has changed.  We could probably get away with not sending the
	 *                        proper signals here but it would technically be possible for a view to detect the
	 *                        inconsistency so we err on the safe side.
	 *
	 *   2 - Child exposed:   this is the state that the ListTracker enters after a child has been exposed (ie:
	 *                        get_child() has been called).  In this state, the ListTracker has created the
	 *                        appropriate number of DictionaryTracker children and is holding reference to them
	 *                        in both linked list and array form.  Changes are signaled as per the previous
	 *                        state.
	 *
	 * The change between state 0 and state 1 is signaled by the setting of the 'length_exposed' flag to true.
	 * The change between state 1 and state 2 is signaled by setting of 'first' and 'array' to non-null values.
	 * Note that the state 2 implies the 'length_exposed' is already set to true since full signals will need to
	 * be sent once any child has been exposed.
	 *
	 * We don't bother with a state where we're not monitoring anything at all because the ListTracker is only
	 * created in the first place when it is required to satisfy a call which is *at least* has_child() at which
	 * point we need to be monitoring the Model.List so that we can emit row_has_child_toggled.
	 */
	class ListTracker {
		/* We store our child DictionaryTrackers in two forms: an array and a linked list.  Each
		 * DictionaryTracker contains a 'next' pointer and the 'first' pointer here always points at the first
		 * one.
		 *
		 * Having only a linked list would cause the following two operations to occur in O(n) time because they
		 * would require traversal of the linked list:
		 *
		 *   - getting a child at a particular index.  This is used for implementing
		 *     GtkTreeModel.iter_nth_child() and Gtk.TreeModel.get_iter (ie: from Gtk.TreePath).
		 *
		 *   - getting the index of a particular child.  This is used for implementing GtkTreeModel.get_path
		 *     (ie: essentially a reverse-lookup from a Gtk.TreeIter).
		 *
		 * The only way to make the first operation occur in O(1) time is to keep an array of the
		 * DictionaryTrackers.  The only way to make the second operation occur in O(1) time is to store within
		 * each DictionaryTracker its own index within its parent.
		 *
		 * Keeping this information at all times would make large change operations on the list very slow since
		 * GtkTreeModel requires emission of delete/insert signals one at a time and the model must be
		 * consistent at the time each signal is emitted (ie: each signal emission would cost O(n) to resize the
		 * array and update the index on each child).
		 *
		 * For this reason, the array is not kept in the midst of change operations and is only recreated after
		 * all changes have been merged and signaled.  During this time 'array' is set to null and linked list
		 * traversal must be used for both forward and reverse lookups.  During times when 'array' is not set to
		 * null then 'array' can be used for forward lookups and the index stored by each DictionaryTracker can
		 * be assumed to be valid.
		 */
		DictionaryTracker[]? array;
		DictionaryTracker? first;

		// The signal handler id for the 'changed' signal on the Model.List
		ulong changed_id;

		// The number of items in the list.  This is always known, even when 'length_exposed' is set to false.
		ulong length;

		/* Weak references to avoid reference cycles.  The parent reference is used for
		 * Gtk.TreeModel.iter_get_parent() and the model reference is used for emitting signals.
		 */
		internal weak DictionaryTracker? parent;
		internal weak TreeModel model;

		// We hold a reference on the list in order to keep it alive so it will keep sending signals to us.
		Model.List? source;

		/* Multiple Model.List change operations tend to be signaled in ascending order of position (as
		 * suggested to implementors by the Model.List API documentation).  In order to avoid re-traversing the
		 * linked list up to the current point, we keep a cache of where we left off last time and what the
		 * position was.  This lets us merely 'seek' to the start of the next group of changes.
		 *
		 * As a side-effect, we can inspect 'cached_child != null' to determine if we are currently in the midst
		 * of a change.  We can use this information to avoid creating 'array' for ourselves during reentrancy
		 * from a signal emission.
		 */
		DictionaryTracker **cached_child;
		ulong cached_child_index;

		// Set to true if the length has been exposed (see overview comment for ListTracker)
		bool length_exposed;

		internal bool has_child () {
			return length > 0;
		}

		internal ulong n_children () {
			length_exposed = true;

			return length;
		}

		internal DictionaryTracker? get_child_for_index (ulong index) {
			length_exposed = true;

			if (index < length) {
				if (first == null) {
					/* length > 0, so we should have at least one child here.
					 * Clearly we are in state 1 and need to move to state 2.
					 */
					create_child_linked_list ();
					assert (first != null);

					/* This could easily be a reentrance from a signal emission.
					 * Only create 'array' if it's not.
					 */
					if (cached_child == null) {
						create_child_array ();
					}
				}

				if (array == null) {
					var link = first;

					while (index --> 0) {
						link = link.next;
					}

					return link;
				} else {
					assert (index < array.length);
					assert (array[index] != null);
					return array[index];
				}
			} else {
				return null;
			}
		}

		internal ulong get_index_for_child (DictionaryTracker child) {
			assert (length_exposed);
			assert (first != null);

			if (array != null) {
				assert (array[child.index] == child);
				return child.index;
			} else {
				var link = first;
				var i = 0;

				while (link != child) {
					link = link.next;
					i++;
				}

				return i;
			}
		}

		internal Model.Dictionary get_dictionary_for_child (DictionaryTracker child) {
			var dictionary = source.get_child (get_index_for_child (child)) as Model.Dictionary;
			assert (dictionary != null);
			return dictionary;
		}

		void create_child_linked_list () {
			for (var i = 0; i < length; i++) {
				first = new DictionaryTracker (this, first);
			}
		}

		void create_child_array () {
			var link = first;

			assert (array == null);

			array = new DictionaryTracker[length];
			for (var i = 0; i < length; i++) {
				array[i] = link;
				link.index = i;

				link = link.next;
			}
			assert (link == null);
		}

		internal ListTracker (TreeModel model, DictionaryTracker? parent, Model.List? source) {
			this.length_exposed = false;
			this.parent = parent;
			this.source = source;
			this.model = model;

			if (source != null) {
				length = source.n_children ();
				changed_id = source.changed.connect (source_changed);
			}
		}

		internal Gtk.TreePath get_tree_path () {
			if (parent == null) {
				return new Gtk.TreePath ();
			}

			return parent.get_tree_path ();
		}

		void check_has_child_toggled (bool was_empty, Gtk.TreePath path) {
			if (was_empty != (length == 0)) {
				Gtk.TreeIter iter;

				if (model.write_iter (out iter, parent)) {
					model.row_has_child_toggled (path, iter);
				}
			}
		}

		void source_changed (Model.List source, ulong position, ulong deleted, ulong inserted, bool more) {
			var was_empty = length == 0;
			var path = get_tree_path ();

			if (!length_exposed) {
				// If the length hasn't been exposed then only signal changes in has_child.
				length += inserted - deleted;
				check_has_child_toggled (was_empty, path);
				return;
			}

			array = null;

			/* seek to position */
			if (cached_child_index > position) {
				cached_child = null;
			}

			if (cached_child == null) {
				cached_child = &first;

				for (cached_child_index = 0; cached_child_index < position; cached_child_index++) {
					cached_child = &((DictionaryTracker) (*cached_child)).next;
				}
			}

			path.append_index ((int) position);

			while (deleted --> 0) {
				model.row_deleted (path);

				var tmp = *cached_child;
				*cached_child = (owned) ((DictionaryTracker) tmp).next;
				delete tmp;
				length--;
			}

			path.up ();

			while (inserted --> 0) {
				*cached_child = new DictionaryTracker (this, *cached_child);
				weak DictionaryTracker dt = *cached_child;
				Gtk.TreeIter iter;

				path.append_index ((int) position++);
				model.write_iter (out iter, dt);
				cached_child = &dt.next;
				length++;

				model.row_inserted (path, iter);

				/* Huge hack alert:
				 *
				 * Although GtkTreeModel allows models to be 'lazy' by not producing data until the view has
				 * asked for it, the API does not allow for the possibility to be lazy about inserting new data.
				 * This is a simple consequence of the fact that all signals must be sent at all times.
				 *
				 * Consider the case of a filesystem model where a new (populated) directory suddenly appears
				 * (as if moved in from another location).  It is not enough to simply send the 'row_inserted'
				 * signal for the directory.  In this case GtkTreeView (at least) assumes that the added
				 * directory is empty and no expander widget is shown.
				 *
				 * The proper way to do this would be to recursively enumerate the new child.  No thanks.
				 *
				 * As a workaround, we can send a 'row_has_child_toggled' signal, even though the view has never
				 * asked for 'has_child' for the new row.  Although at this point the model is now technically
				 * inconsistent (since we didn't signal the insertion of those new children), GtkTreeView seems
				 * to stumble along...
				 */
				if (dt.get_list_tracker ().has_child ()) {
					model.row_has_child_toggled (path, iter);
				}

				path.up ();
			}

			if (more == false) {
				cached_child = null;
				create_child_array ();
			}

			check_has_child_toggled (was_empty, path);
		}

		internal void set_source (Model.List? source) {
			if (length > 0) {
				// Create a synthetic change event that deletes all items
				source_changed (this.source, 0, length, 0, false);
				assert (length == 0);
			}

			if (this.source != null) {
				this.source.disconnect (changed_id);
			}

			this.source = source;

			if (source != null) {
				if (length_exposed) {
					source_changed (source, 0, 0, source.n_children (), false);
				} else {
					length = source.n_children ();
				}

				changed_id = source.changed.connect (source_changed);
			}
		}

		~ListTracker () {
			if (source != null) {
				source.disconnect (changed_id);
			}
		}
	}

	/* The DictionaryTracker is responsible for tracking a Model.Dictionary in a way that is convenient for a
	 * GtkTreeModel implementation.  It attempts to be lazy, minimising work where possible and only sending
	 * signals for things that an outside observer can know has changed.  For example, if an outside observer
	 * has never requested the value of any key of the dictionary then the DictionaryTracker won't be watching
	 * for changes in that value.
	 *
	 * To this end, there are three states that a DictionaryTracker can be in:
	 *
	 *   0 - Freshly created: this is the initial state.  The DictionaryTracker is essentially an empty shell at
	 *                        this point.  It doesn't even contain a reference to the Model.Dictionary that it
	 *                        represents.
	 *
	 *   1 - Value exposed:   this is the state that the DictionaryTracker enters after any requests have been
	 *                        made against it requesting the value of a key in the dictionary (including the
	 *                        "children" key).  In this state the DictionaryTracker holds reference to the
	 *                        underlying Model.Reference for each value and watches for changes on them.
	 *
	 *   2 - List exposed:    this is the state that the DictionaryTracker enters after any request has been
	 *                        made for the ListTracker for the children of the dictionary.
	 *
	 * The model is in the 'value exposed' state if the 'references' array is non-null.  The model is in the
	 * 'list exposed' state if 'children' is non-null.
	 *
	 * The DictionaryTracker is deliberately antagonistic in that it will not ever hold a reference on the
	 * Model.Dictionary that it is tracking -- only the Model.Reference for each value of the dictionary.  This
	 * can expose bugs in model implementations, particularly with respect to change notifications.
	 **/
	class DictionaryTracker {
		// These are both null until some amount of data has been accessed
		Model.Reference[]? references;
		ulong[]? reference_handlers;

		// This is null until the list tracker has been accessed.
		ListTracker? children;

		// Linkage...
		internal weak ListTracker siblings;
		internal DictionaryTracker? next;

		/* It is not valid to access this directly.  It is only set to the correct value in the case that
		 * 'siblings.array' is non-null.  The code to check this and to calculate the value (by traversal) if
		 * needed lives in ListTracker, so siblings.get_index_for_child() should be used.
		 */
		internal ulong index;

		void reference_changed (Model.Reference reference) {
			if (reference == references[0] && children != null) {
				children.set_source (reference.get_value () as Model.List);
			}

			var path = get_tree_path ();
			Gtk.TreeIter iter;

			siblings.model.write_iter (out iter, this);
			siblings.model.row_changed (path, iter);
		}

		internal ListTracker get_list_tracker () {
			if (children == null) {
				children = new ListTracker (siblings.model, this, get_value (0) as Model.List);
			}

			return children;
		}

		internal Model.Object get_value (int index) {
			if (references == null) {
				setup_references ();
			}

			return references[index].get_value ();
		}

		void setup_references () {
			var source = siblings.get_dictionary_for_child (this);
			var keys = siblings.model.get_keys ();

			references = new Model.Reference[keys.length];
			reference_handlers = new ulong[keys.length];

			for (var i = 0; i < keys.length; i++) {
				references[i] = source.get_reference (keys[i]);
				reference_handlers[i] = references[i].changed.connect (reference_changed);
			}
		}

		public DictionaryTracker (ListTracker siblings, DictionaryTracker? next) {
			this.siblings = siblings;
			this.next = next;
		}

		internal Gtk.TreePath get_tree_path () {
			var path = siblings.get_tree_path ();
			path.append_index ((int) siblings.get_index_for_child (this));
			return path;
		}

		~DictionaryTracker () {
			for (var i = 0; i < references.length; i++) {
				references[i].disconnect (reference_handlers[i]);
			}
		}
	}

	public class TreeModel : GLib.Object, Gtk.TreeModel {
		bool list_only;
		string[] keys;
		Type[] types;

		ListTracker root;
		int stamp;

		GLib.Type get_column_type (int index) {
			return types[index];
		}

		Gtk.TreeModelFlags get_flags () {
			Gtk.TreeModelFlags flags;

			flags = Gtk.TreeModelFlags.ITERS_PERSIST;

			if (list_only) {
				flags |= Gtk.TreeModelFlags.LIST_ONLY;
			}

			return flags;
		}

		/* workaround for poor binding of Gtk.TreePath */
		unowned int[] get_tree_path_indices (Gtk.TreePath path) {
			weak int[] indices = path.get_indices ();
			indices.length = path.get_depth ();
			return indices;
		}

		bool get_iter (out Gtk.TreeIter iter, Gtk.TreePath path) {
			var indices = get_tree_path_indices (path);
			DictionaryTracker? dt;
			dt = root.get_child_for_index (indices[0]);
			for (var i = 1; dt != null && i < indices.length; i++) {
				dt = dt.get_list_tracker ().get_child_for_index (indices[i]);
			}
			
			return write_iter (out iter, dt);
		}

		int get_n_columns () {
			return keys.length;
		}

		Gtk.TreePath? get_path (Gtk.TreeIter iter) {
			return iter_to_dictionary_tracker (iter).get_tree_path ();
		}

		void get_value (Gtk.TreeIter iter, int column, out GLib.Value value) {
			var dt = iter_to_dictionary_tracker (iter);
			var object = dt.get_value (column);

			value.init (types[column]);

			if (types[column] == typeof (string)) {
				var string_object = object as Model.String;
				if (string_object != null) {
					value.set_string (string_object.get ());
				}
			} else if (types[column] == typeof (int)) {
				var integer_object = object as Model.Integer;
				if (integer_object != null) {
					value.set_int (integer_object.get ());
				}
			} else if (types[column] == typeof (double)) {
				var float_object = object as Model.Float;
				if (float_object != null) {
					value.set_double (float_object.get ());
				}
			} if (types[column] == typeof (bool)) {
				var boolean_object = object as Model.Boolean;
				if (boolean_object != null) {
					value.set_boolean (boolean_object.get ());
				}
			}
		}

		DictionaryTracker iter_to_dictionary_tracker (Gtk.TreeIter iter) {
			assert (iter.stamp == stamp);
			assert (iter.user_data != null);

			return (DictionaryTracker) iter.user_data;
		}

		ListTracker? iter_to_list_tracker (Gtk.TreeIter? iter) {
			if (iter != null) {
				return iter_to_dictionary_tracker (iter).get_list_tracker ();
			} else {
				return root;
			}
		}

		internal bool write_iter (out Gtk.TreeIter iter, DictionaryTracker? dt) {
			iter.stamp = (dt != null) ? stamp : 0;
			iter.user_data = (void *) dt;

			return dt != null;
		}

		internal unowned string[] get_keys () {
			return keys;
		}

		bool iter_children (out Gtk.TreeIter iter, Gtk.TreeIter? parent) {
			var lt = iter_to_list_tracker (parent);
			return write_iter (out iter, lt == null ? null : lt.get_child_for_index (0));
		}

		bool iter_has_child (Gtk.TreeIter iter) {
			var lt = iter_to_list_tracker (iter);
			return lt != null && lt.has_child ();
		}

		int iter_n_children (Gtk.TreeIter? iter) {
			var lt = iter_to_list_tracker (iter);
			return lt == null ? 0 : (int) lt.n_children ();
		}

		bool iter_next (ref Gtk.TreeIter iter) {
			var dt = iter_to_dictionary_tracker (iter);
			return write_iter (out iter, dt.next);
		}

		bool iter_nth_child (out Gtk.TreeIter iter, Gtk.TreeIter? parent, int n) {
			var lt = iter_to_list_tracker (parent);
			return write_iter (out iter, lt == null ? null : lt.get_child_for_index (n));
		}

		bool iter_parent (out Gtk.TreeIter iter, Gtk.TreeIter child) {
			var dt = iter_to_dictionary_tracker (child);
			return write_iter (out iter, dt.siblings.parent);
		}

		void ref_node (Gtk.TreeIter iter) {
		}

		void unref_node (Gtk.TreeIter iter) {
		}

		public TreeModel (Model.List list, int num_columns,
		                  [CCode (array_length = false)] string **keys,
		                  [CCode (array_length = false)] Type *types,
		                  bool list_only) {
			assert (list_only || (num_columns > 0 && types[0] == typeof (Model.List)));

			weak Type[] mytypes = (Type[]) types;
			mytypes.length = num_columns;
			this.types = mytypes;

			weak string[] mykeys = (string[]) keys;
			mykeys.length = num_columns;
			this.keys = mykeys;

			this.root = new ListTracker (this, null, list);
			this.stamp = (int) Random.next_int ();
			this.list_only = list_only;
		}
	}
}

// vim:ts=4 sw=4 noet:
