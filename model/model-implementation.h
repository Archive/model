/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _libmodel_model_implementation_h_
#define _libmodel_model_implementation_h_

#include "model.h"

G_BEGIN_DECLS

ModelObject *           model_string_new                                (const gchar             *value);
ModelObject *           model_integer_new                               (gint                     value);
ModelObject *           model_float_new                                 (gdouble                  value);
ModelObject *           model_boolean_new                               (gboolean                 value);

void                    model_reference_changed                         (ModelReference          *reference);

void                    model_list_changed                              (ModelList               *list,
                                                                         gulong                   position,
                                                                         gulong                   removed,
                                                                         gulong                   inserted,
                                                                         gboolean                 more);

#define MODEL_TYPE_SIMPLE_REFERENCE                         model_simple_reference_get_type ()
#define MODEL_SIMPLE_REFERENCE(inst)                        (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_REFERENCE, ModelSimpleReference))
#define MODEL_IS_SIMPLE_REFERENCE(inst)                     (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_REFERENCE))

typedef struct _ModelSimpleReference                        ModelSimpleReference;

GType                   model_simple_reference_get_type                 (void);
ModelSimpleReference *  model_simple_reference_new                      (ModelObject             *value);
ModelSimpleReference *  model_simple_reference_new_string               (const gchar             *value);
ModelSimpleReference *  model_simple_reference_new_integer              (gint                     value);
ModelSimpleReference *  model_simple_reference_new_float                (gdouble                  value);
ModelSimpleReference *  model_simple_reference_new_boolean              (gboolean                 value);
void                    model_simple_reference_set                      (ModelSimpleReference    *simple,
                                                                         ModelObject             *value);
void                    model_simple_reference_set_string               (ModelSimpleReference    *simple,
                                                                         const gchar             *value);
void                    model_simple_reference_set_integer              (ModelSimpleReference    *simple,
                                                                         gint                     value);
void                    model_simple_reference_set_float                (ModelSimpleReference    *simple,
                                                                         gdouble                  value);
void                    model_simple_reference_set_boolean              (ModelSimpleReference    *simple,
                                                                         gboolean                 value);


#define MODEL_TYPE_REFERENCE_HELPER                         model_reference_helper_get_type ()
#define MODEL_REFERENCE_HELPER(inst)                        (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_REFERENCE_HELPER, ModelReferenceHelper))
#define MODEL_IS_REFERENCE_HELPER(inst)                     (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_REFERENCE_HELPER))

typedef struct _ModelReferenceHelper                        ModelReferenceHelper;

GType                   model_reference_helper_get_type                 (void);
ModelReference *        model_reference_helper_get_reference            (ModelReferenceHelper    *helper);
ModelReferenceHelper *  model_reference_helper_new                      (GObject                 *owner,
                                                                         ModelObject             *value);
ModelReferenceHelper *  model_reference_helper_new_string               (GObject                 *owner,
                                                                         const gchar             *value);
ModelReferenceHelper *  model_reference_helper_new_integer              (GObject                 *owner,
                                                                         gint                     value);
ModelReferenceHelper *  model_reference_helper_new_float                (GObject                 *owner,
                                                                         gdouble                  value);
ModelReferenceHelper *  model_reference_helper_new_boolean              (GObject                 *owner,
                                                                         gboolean                 value);
void                    model_reference_helper_set                      (ModelReferenceHelper    *helper,
                                                                         ModelObject             *value);
void                    model_reference_helper_set_string               (ModelReferenceHelper    *helper,
                                                                         const gchar             *value);
void                    model_reference_helper_set_integer              (ModelReferenceHelper    *helper,
                                                                         gint                     value);
void                    model_reference_helper_set_float                (ModelReferenceHelper    *helper,
                                                                         gdouble                  value);
void                    model_reference_helper_set_boolean              (ModelReferenceHelper    *helper,
                                                                         gboolean                 value);

#define MODEL_TYPE_SIMPLE_DICTIONARY                        model_simple_dictionary_get_type ()
#define MODEL_SIMPLE_DICTIONARY(inst)                       (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_DICTIONARY,                           \
                                                             ModelSimpleDictionary))
#define MODEL_SIMPLE_DICTIONARY_CLASS(class)                (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_SIMPLE_DICTIONARY,                           \
                                                             ModelSimpleDictionaryClass))
#define MODEL_IS_SIMPLE_DICTIONARY(inst)                    (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_DICTIONARY))
#define MODEL_IS_SIMPLE_DICTIONARY_CLASS(class)             (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_SIMPLE_DICTIONARY))
#define MODEL_SIMPLE_DICTIONARY_GET_CLASS(inst)             (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_SIMPLE_DICTIONARY,                           \
                                                             ModelSimpleDictionaryClass))

typedef struct _ModelSimpleDictionaryPrivate                ModelSimpleDictionaryPrivate;
typedef struct _ModelSimpleDictionaryClass                  ModelSimpleDictionaryClass;
typedef struct _ModelSimpleDictionary                       ModelSimpleDictionary;

struct _ModelSimpleDictionaryClass
{
  ModelDictionaryClass parent_class;
};

struct _ModelSimpleDictionary
{
  ModelDictionary parent_instance;

  /*< private >*/
  ModelSimpleDictionaryPrivate *priv;
};

GType                   model_simple_dictionary_get_type                (void);
ModelSimpleDictionary * model_simple_dictionary_new                     (void);
void                    model_simple_dictionary_set                     (ModelSimpleDictionary   *simple,
                                                                         const gchar             *key,
                                                                         ModelObject             *value,
                                                                         gboolean                 monitored);
void                    model_simple_dictionary_set_boolean             (ModelSimpleDictionary   *simple,
                                                                         const gchar             *key,
                                                                         gboolean                 value,
                                                                         gboolean                 monitored);
void                    model_simple_dictionary_set_integer             (ModelSimpleDictionary   *simple,
                                                                         const gchar             *key,
                                                                         gint                     value,
                                                                         gboolean                 monitored);
void                    model_simple_dictionary_set_float               (ModelSimpleDictionary   *simple,
                                                                         const gchar             *key,
                                                                         gdouble                  value,
                                                                         gboolean                 monitored);
void                    model_simple_dictionary_set_string              (ModelSimpleDictionary   *simple,
                                                                         const gchar             *key,
                                                                         const gchar             *value,
                                                                         gboolean                 monitored);

#define MODEL_TYPE_SIMPLE_LIST                               model_simple_list_get_type ()
#define MODEL_SIMPLE_LIST(inst)                             (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_LIST, ModelSimpleList))
#define MODEL_IS_SIMPLE_LIST(inst)                          (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_SIMPLE_LIST))

typedef struct _ModelSimpleList                             ModelSimpleList;

GType                   model_simple_list_get_type                      (void);
ModelSimpleList *       model_simple_list_new                           (void);
void                    model_simple_list_splice                        (ModelSimpleList         *list,
                                                                         gulong                   position,
                                                                         gulong                   n_removes,
                                                                         ModelObject * const     *inserts,
                                                                         gulong                   n_inserts,
                                                                         gboolean                 more);
void                    model_simple_list_remove                        (ModelSimpleList         *list,
                                                                         gulong                   position);
void                    model_simple_list_insert                        (ModelSimpleList         *list,
                                                                         gulong                   position,
                                                                         ModelObject             *value);
void                    model_simple_list_append                        (ModelSimpleList         *list,
                                                                         ModelObject             *value);



#define MODEL_TYPE_ABSTRACT_SORTED_LIST                     model_abstract_sorted_list_get_type ()
#define MODEL_ABSTRACT_SORTED_LIST(inst)                    (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_ABSTRACT_SORTED_LIST,                        \
                                                             ModelAbstractSortedList))
#define MODEL_ABSTRACT_SORTED_LIST_CLASS(class)             (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_ABSTRACT_SORTED_LIST,                        \
                                                             ModelAbstractSortedListClass))
#define MODEL_IS_ABSTRACT_SORTED_LIST(inst)                 (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_ABSTRACT_SORTED_LIST))
#define MODEL_IS_ABSTRACT_SORTED_LIST_CLASS(class)          (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_ABSTRACT_SORTED_LIST))
#define MODEL_ABSTRACT_SORTED_LIST_GET_CLASS(inst)          (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_ABSTRACT_SORTED_LIST,                        \
                                                             ModelAbstractSortedListClass))

typedef struct _ModelAbstractSortedListPrivate              ModelAbstractSortedListPrivate;
typedef struct _ModelAbstractSortedListClass                ModelAbstractSortedListClass;
typedef struct _ModelAbstractSortedList                     ModelAbstractSortedList;

struct _ModelAbstractSortedListClass
{
  ModelListClass parent_class;

  gint (*compare)     (ModelAbstractSortedList *list,
                       gconstpointer            a,
                       gconstpointer            b);

  void (*warning)     (ModelAbstractSortedList *list,
                       gulong                   index,
                       gconstpointer            key,
                       gchar                    mode,
                       gulong                   current_index,
                       ModelObject             *value);

  void (*create_item) (ModelAbstractSortedList  *list,
                       gulong                    index,
                       gconstpointer             key,
                       gpointer                 *new_key,
                       ModelObject             **new_object);

  void (*free_key)    (ModelAbstractSortedList *list,
                       gpointer                 key);
};

struct _ModelAbstractSortedList
{
  ModelList parent_instance;

  /*< private >*/
  ModelAbstractSortedListPrivate *priv;
};

GType                   model_abstract_sorted_list_get_type             (void);
void                    model_abstract_sorted_list_merge                (ModelAbstractSortedList *sorted,
                                                                         const gchar             *mode,
                                                                         const gconstpointer     *keys,
                                                                         gulong                   n_keys);
G_END_DECLS

#endif /* _libmodel_model_implementation_h_ */
