[CCode (cheader_filename = "model-implementation.h", gir_namespace = "Model", gir_version = "0.2")]
namespace Model {
  public abstract class Object : GLib.Object { }

  public abstract class Reference : GLib.Object {
    public abstract Model.Object get_value ();
    [HasEmitter]
    public signal void changed ();
  }


  public class Boolean : Model.Object {
    [CCode (type = "ModelObject *")]
    public Boolean (bool value);
    public bool @get ();
  }

  public class Integer : Model.Object {
    [CCode (type = "ModelObject *")]
    public Integer (int value);
    public int @get ();
  }

  public class String : Model.Object {
    [CCode (type = "ModelObject *")]
    public String (string value);
    public string @get ();
    public unowned string peek ();
  }

  public class Float : Model.Object {
    [CCode (type = "ModelObject *")]
    public Float (double value);
    public double @get ();
  }


  public abstract class Dictionary : Model.Object {
    public abstract Model.Reference get_reference (string key);
    public abstract Model.Object get_value (string key);
    public abstract string[] list_keys ();
  }

  public abstract class List : Model.Object {
    public abstract Model.Object get_child (ulong index);
    public abstract ulong n_children ();
    [HasEmitter]
    public signal void changed (ulong position, ulong removed,
                                ulong inserted, bool more);
  }

  public class SimpleReference : Model.Reference {
    public SimpleReference (Model.Object value);
    public void @set (Model.Object value);
  }

  public class ReferenceHelper : GLib.Object {
    public ReferenceHelper (GLib.Object owner, Model.Object value);
    public void @set (Model.Object? value);

    public ReferenceHelper.boolean (GLib.Object owner, bool value);
    public ReferenceHelper.float (GLib.Object owner, double value);
    public ReferenceHelper.integer (GLib.Object owner, int value);
    public ReferenceHelper.string (GLib.Object owner, string value);

    public void set_boolean (bool value);
    public void set_float (double value);
    public void set_integer (int value);
    public void set_string (string value);
  }

  public class SimpleDictionary : Model.Dictionary {
    [CCode (has_construct_function = false)]
    public SimpleDictionary ();
    public void set (string key, Object? value, bool monitored = true);
    public void set_integer (string key, int value, bool monitored = true);
    public void set_string (string key, string value, bool monitored = true);
    public void set_float (string key, double value, bool monitored = true);
    public void set_boolean (string key, bool value, bool monitored = true);
  }

  public class SimpleList : Model.List {
    public SimpleList ();
    public void splice (ulong position, ulong n_removes, Object[] inserts);
    public void remove (ulong position);
    public void insert (ulong position, Object value);
    public void append (Object value);
  }

  public abstract class AbstractSortedList : Model.List {
    [SimpleType] [CCode (cname = "gpointer")]
    public struct Key { }
    [SimpleType] [CCode (cname = "gconstpointer")]
    public struct ConstKey { }

    public override Model.Object get_child (ulong index);
    public override ulong n_children ();

    [NoWrapper]
    protected abstract int compare (ConstKey a, ConstKey b);
    [NoWrapper]
    protected abstract void warning (ulong index, ConstKey key, char mode,
                                     ulong current_index, Object value);
    [NoWrapper]
    protected abstract void create_item (ulong index, ConstKey key,
                                         out Key new_key,
                                         out Object new_value);
    [NoWrapper]
    protected abstract void free_key (Key key);

    public void merge (string mode, ConstKey[] keys);
  }
}
