/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _libmodel_model_h_
#define _libmodel_model_h_

#include <glib-object.h>

G_BEGIN_DECLS

#define MODEL_TYPE_OBJECT                                   model_object_get_type ()
#define MODEL_OBJECT(inst)                                  (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_OBJECT, ModelObject))
#define MODEL_OBJECT_CLASS(class)                           (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_OBJECT, ModelObjectClass))
#define MODEL_IS_OBJECT(inst)                               (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_OBJECT))
#define MODEL_IS_OBJECT_CLASS(class)                        (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_OBJECT))
#define MODEL_OBJECT_GET_CLASS(inst)                        (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_OBJECT, ModelObjectClass))

typedef struct _ModelObjectPrivate                          ModelObjectPrivate;
typedef struct _ModelObjectClass                            ModelObjectClass;
typedef struct _ModelObject                                 ModelObject;

struct _ModelObjectClass
{
  GObjectClass parent_class;
};

struct _ModelObject
{
  GObject parent_instance;

  /*< private >*/
  ModelObjectPrivate *priv;
};

#define MODEL_TYPE_REFERENCE                                model_reference_get_type ()
#define MODEL_REFERENCE(inst)                               (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_REFERENCE, ModelReference))
#define MODEL_REFERENCE_CLASS(class)                        (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_REFERENCE, ModelReferenceClass))
#define MODEL_IS_REFERENCE(inst)                            (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_REFERENCE))
#define MODEL_IS_REFERENCE_CLASS(class)                     (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_REFERENCE))
#define MODEL_REFERENCE_GET_CLASS(inst)                     (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_REFERENCE, ModelReferenceClass))

typedef struct _ModelReferencePrivate                       ModelReferencePrivate;
typedef struct _ModelReferenceClass                         ModelReferenceClass;
typedef struct _ModelReference                              ModelReference;

struct _ModelReferenceClass
{
  GObjectClass parent_class;

  ModelObject * (*get_value) (ModelReference *reference);
};

struct _ModelReference
{
  GObject parent_instance;

  /*< private >*/
  ModelReferencePrivate *priv;
};

#define MODEL_TYPE_DICTIONARY                               model_dictionary_get_type ()
#define MODEL_DICTIONARY(inst)                              (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_DICTIONARY, ModelDictionary))
#define MODEL_DICTIONARY_CLASS(class)                       (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_DICTIONARY, ModelDictionaryClass))
#define MODEL_IS_DICTIONARY(inst)                           (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_DICTIONARY))
#define MODEL_IS_DICTIONARY_CLASS(class)                    (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_DICTIONARY))
#define MODEL_DICTIONARY_GET_CLASS(inst)                    (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_DICTIONARY, ModelDictionaryClass))

typedef struct _ModelDictionaryPrivate                      ModelDictionaryPrivate;
typedef struct _ModelDictionaryClass                        ModelDictionaryClass;
typedef struct _ModelDictionary                             ModelDictionary;

struct _ModelDictionaryClass
{
  ModelObjectClass parent_class;

  ModelReference * (*get_reference) (ModelDictionary *dictionary,
                                     const gchar     *key);
  ModelObject *    (*get_value)     (ModelDictionary *dictionary,
                                     const gchar     *key);
  gchar **         (*list_keys)     (ModelDictionary *dictionary,
                                     gint            *length);
};

struct _ModelDictionary
{
  ModelObject parent_instance;

  /*< private >*/
  ModelDictionaryPrivate *priv;
};

#define MODEL_TYPE_LIST                                     model_list_get_type ()
#define MODEL_LIST(inst)                                    (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_LIST, ModelList))
#define MODEL_LIST_CLASS(class)                             (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             MODEL_TYPE_LIST, ModelListClass))
#define MODEL_IS_LIST(inst)                                 (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_LIST))
#define MODEL_IS_LIST_CLASS(class)                          (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             MODEL_TYPE_LIST))
#define MODEL_LIST_GET_CLASS(inst)                          (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             MODEL_TYPE_LIST, ModelListClass))

typedef struct _ModelListPrivate                            ModelListPrivate;
typedef struct _ModelListClass                              ModelListClass;
typedef struct _ModelList                                   ModelList;

struct _ModelListClass
{
  ModelObjectClass parent_class;

  gulong        (*n_children) (ModelList *list);
  ModelObject * (*get_child)  (ModelList *list,
                               gulong     index);
};

struct _ModelList
{
  ModelObject parent_instance;

  /*< private >*/
  ModelListPrivate *priv;
};

#define MODEL_TYPE_STRING                                   model_string_get_type ()
#define MODEL_STRING(inst)                                  (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_STRING, ModelString))
#define MODEL_IS_STRING(inst)                               (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_STRING))

typedef struct _ModelString                                 ModelString;

#define MODEL_TYPE_INTEGER                                  model_integer_get_type ()
#define MODEL_INTEGER(inst)                                 (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_INTEGER, ModelInteger))
#define MODEL_IS_INTEGER(inst)                              (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_INTEGER))

typedef struct _ModelInteger                                ModelInteger;

#define MODEL_TYPE_FLOAT                                    model_float_get_type ()
#define MODEL_FLOAT(inst)                                   (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_FLOAT, ModelFloat))
#define MODEL_IS_FLOAT(inst)                                (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_FLOAT))

typedef struct _ModelFloat                                  ModelFloat;

#define MODEL_TYPE_BOOLEAN                                  model_boolean_get_type ()
#define MODEL_BOOLEAN(inst)                                 (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             MODEL_TYPE_BOOLEAN, ModelBoolean))
#define MODEL_IS_BOOLEAN(inst)                              (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             MODEL_TYPE_BOOLEAN))

typedef struct _ModelBoolean                                ModelBoolean;


GType                   model_object_get_type                           (void);

GType                   model_reference_get_type                        (void);
ModelObject *           model_reference_get_value                       (ModelReference  *reference);

GType                   model_dictionary_get_type                       (void);
ModelReference *        model_dictionary_get_reference                  (ModelDictionary *dictionary,
                                                                         const gchar     *key);
ModelObject *           model_dictionary_get_value                      (ModelDictionary *dictionary,
                                                                         const gchar     *key);
gchar **                model_dictionary_list_keys                      (ModelDictionary *dictionary,
                                                                         gint            *length);

GType                   model_list_get_type                             (void);
gulong                  model_list_n_children                           (ModelList       *list);
ModelObject *           model_list_get_child                            (ModelList       *list,
                                                                         gulong           index);

GType                   model_string_get_type                           (void);
gchar *                 model_string_get                                (ModelString     *object);
const gchar *           model_string_peek                               (ModelString     *object);

GType                   model_integer_get_type                          (void);
gint                    model_integer_get                               (ModelInteger    *object);

GType                   model_boolean_get_type                          (void);
gboolean                model_boolean_get                               (ModelBoolean    *object);

GType                   model_float_get_type                            (void);
gdouble                 model_float_get                                 (ModelFloat      *object);

G_END_DECLS

#endif /* _libmodel_model_h_ */
